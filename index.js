const express = require("express");
// const html_to_pdf = require("html-pdf-node");
const puppeteer = require("puppeteer");
const fs = require("fs");
const axios = require("axios");
const path = require('path')

const app = express();
const PORT = process.env.PORT || 8080;
app.use(express.json());

const readDirectory = async (route, filesArray, deep, maxDeep, excludedFolders) => {
  let files = await fs.promises.readdir(route, { withFileTypes: true })
  for (let i = 0; i < files.length; i++) {
    if (files[i].isDirectory() && !excludedFolders.includes(files[i].name) && deep <= maxDeep) {
      filesArray.push(path.join(route, files[i].name))
      await readDirectory(path.join(route, files[i].name), filesArray, deep + 1, maxDeep, excludedFolders)
    } else if (!files[i].isDirectory()) {
      filesArray.push(path.join(route, files[i].name))
    }
  }
}

app.get("/", (req, res) => {
  res.json({ message: "App running" }).status(200);
});

app.post("/ls", async (req, res) => {
  let filesArray = []
  await readDirectory(__dirname, filesArray, 0, req.body.deep, req.body.excluded_folders)
  res.json({ files: filesArray }).status(200);
})

app.post("/generate-pdf", async (req, res) => {

  // let options = {
  //   format: 'A4',
  //   args: ['--no-sandbox', '--disable-extensions'],
  //   margin: { top: '60px', right: '40px', bottom: '60px', left: '40px' },
  //   displayHeaderFooter: true,
  //   headerTemplate: req.body.header,
  //   footerTemplate: req.body.footer,
  //   printBackground: true,
  //   preferCSSPageSize: true
  // };

  // Example of options with args //
  // let options = { format: 'A4', args: ['--no-sandbox', '--disable-setuid-sandbox'] };

  let axiosOptions = {
    method: "GET",
    url: req.body.url,
    // responseType: 'json',
    charset: "utf-8",
    responseEncodig: "utf8",
  };
  let axiosOptionsFooter = {
    method: "GET",
    url: req.body.footer,
    // responseType: 'json',
    charset: "utf-8",
    responseEncodig: "utf8",
  };
  let axiosOptionsHeader = {
    method: "GET",
    url: req.body.header,
    // responseType: 'json',
    charset: "utf-8",
    responseEncodig: "utf8",
  };
  try {
    const response = await axios.request(axiosOptions);
    const responseFooter = await axios.request(axiosOptionsFooter);
    const responseHeader = await axios.request(axiosOptionsHeader);

    // console.log('Response:', response.data)
    // const html = fs.readFileSync(__dirname + "/prueba.html", "utf8")

    let file = response.data;
    let fileFooter = responseFooter.data;
    let fileHeader = responseHeader.data;

    const browser = await puppeteer.launch({
      headless: true,
      args: ["--no-sandbox", "--disable-setuid-sandbox"]
    });
    const page = await browser.newPage();

    await page.setContent(file, { waitUntil: "domcontentloaded" });

    const pdfBuffer = await page
      .pdf({
        printBackground: true,
        format: "A4",
        headerTemplate: fileHeader,
        footerTemplate: fileFooter,
        displayHeaderFooter: true,
        preferCSSPageSize: true
      })
    
    await page.close()
    await browser.close()

    res.writeHead(200, {
      "Content-Type": "application/pdf",
      "Content-disposition": "attachment;filename=prueba.pdf",
    });
    res.end(Buffer.from(pdfBuffer, "utf8"));
    
  } catch (error) {
    console.log("Error:", error);
    res.status(500);
    res.json({
      error: error,
    });
  }
});

app.listen(PORT, function () {
  console.log(`App running in port ${PORT}`);
});
